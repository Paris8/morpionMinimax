# Morpion / Tic-Tac-Toe

Implémentation d'un Morpion d'une taille variable aussi bien en hauteur qu'en largeur avec une implémentation de l'algorithme "Minimax" vu en cours.


## Usage

```bash
python3 minimax.py <args>
```
Le programme s'adapte aux arguments données :
 - Si un argument → Précisez la taille du tableau, exemple : `3x3`.
 - Si deux arguments → Précisez le nom des deux joueurs qui vont jouer *(Un joueur = un charactère)*.
 - Si trois arguments → Précisez alors la taille le nom des deux joueurs et la taille du tableau, en suivant les règles précédentes.
